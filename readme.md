# Kata Convertisseur de nombre en toutes lettres

## Instructions

Ecrire un algorithme qui prend en entrée un nombre entier entre 0 et 999 et retourne le même nombre écrit en toutes
lettres.

## Règles

* En orthographe moderne, on relie par un trait d’union tous les éléments d’un numéral composé.  
  Exemples :
  dix-sept (17), quarante-cinq (45), soixante-dix-huit (78), vingt-et-un (21), vingt-deux (22), cent-trois (103),
  trente-trois (33)
* Les mots vingt et cent prennent la marque du pluriel à trois conditions :
    - ils doivent être multipliés (cinq-cents = 5 x 100)
    - ils doivent terminer le nombre (quatre-vingts, mais quatre-vingt-sept)  
  Exemples : cent-vingt (120), trois-cent-quatre-vingt-dix (390)

On écrira donc : cent-vingt (120) sans « s », car cent et vingt ne sont pas multipliés; trois-cent-quatre-vingt-dix (390) parce que cent et vingt ne terminent pas le nombre 