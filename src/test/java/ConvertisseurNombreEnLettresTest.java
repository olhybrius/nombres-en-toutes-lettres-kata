

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class ConvertisseurNombreEnLettresTest {

    @ParameterizedTest
    @CsvSource(delimiter = '|', textBlock = """
            0|zero
            1|un
            2|deux
            3|trois
            17| dix-sept
            18| dix-huit
            19| dix-neuf
            20| vingt
            21| vingt-et-un
            22| vingt-deux
            23| vingt-trois
            30| trente
            58| cinquante-huit
            64| soixante-quatre
            74| soixante-quatorze
            79| soixante-dix-neuf
            80| quatre-vingts
            87| quatre-vingt-sept
            96| quatre-vingt-seize
            97| quatre-vingt-dix-sept
            100| cent
            101| cent-un
            112| cent-douze
            146| cent-quarante-six
            173| cent-soixante-treize
            199| cent-quatre-vingt-dix-neuf
            500| cinq-cents
            999| neuf-cent-quatre-vingt-dix-neuf
                """)
    void passer_1_retourne_un(int nombre, String nombreEnLettresAttendu) {
        ConvertisseurNombreEnLettres convertisseurNombreEnLettres = new ConvertisseurNombreEnLettres();
        String nombreEnLettres = convertisseurNombreEnLettres.convertir(nombre);
        assertThat(nombreEnLettresAttendu).isEqualTo(nombreEnLettres);
    }
}
