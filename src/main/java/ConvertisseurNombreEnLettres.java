import java.util.Map;

import static java.util.Map.entry;

public class ConvertisseurNombreEnLettres {
    private final Map<Integer, String> chiffresEnLettres = Map.ofEntries(
            entry(1, "un"),
            entry(2, "deux"),
            entry(3, "trois"),
            entry(4, "quatre"),
            entry(5, "cinq"),
            entry(6, "six"),
            entry(7, "sept"),
            entry(8, "huit"),
            entry(9, "neuf"),
            entry(10, "dix"),
            entry(11, "onze"),
            entry(12, "douze"),
            entry(13, "treize"),
            entry(14, "quatorze"),
            entry(15, "quinze"),
            entry(16, "seize")
    );

    private final Map<Integer, String> dizainesReguliers = Map.ofEntries(
            entry(2, "vingt"),
            entry(3, "trente"),
            entry(4, "quarante"),
            entry(5, "cinquante"),
            entry(6, "soixante"),
            entry(8, "quatre-vingt")
    );

    private final Map<Integer, String> dizainesIrreguliers = Map.ofEntries(
            entry(7, "soixante"),
            entry(9, "quatre-vingt")
    );

    public String convertir(int nombre) {
        if (nombre == 0) return "zero";

        String enLettres = "";
        String nombreToString = String.valueOf(nombre);

        if (nombreToString.length() == 3) {
            int chiffreCentaines = getChiffre(nombreToString, POSITION.CENTAINES);
            if (chiffreCentaines > 1 && chiffresEnLettres.containsKey(chiffreCentaines)) {
                enLettres += chiffresEnLettres.get(chiffreCentaines) + "-";
            }
            enLettres += "cent";
            if (nombre % 100 == 0 && nombre / 100 > 1) enLettres += "s";
            if (!nombreToString.endsWith("00"))
                enLettres += "-";
            else
                return enLettres;
        }

        int deuxDerniersChiffres = Integer.parseInt(nombreToString.substring(Math.max(nombreToString.length() - 2, 0)));
        if (chiffresEnLettres.containsKey(deuxDerniersChiffres))
            enLettres += chiffresEnLettres.get(deuxDerniersChiffres);
        else {
            int chiffreDizaines = getChiffre(nombreToString, POSITION.DIZAINES);
            int chiffreUnites = getChiffre(nombreToString, POSITION.UNITES);

            if (dizainesIrreguliers.containsKey(chiffreDizaines)) {
                if (!chiffresEnLettres.containsKey(chiffreUnites + 10))
                    enLettres += String.join("-dix-", dizainesIrreguliers.get(chiffreDizaines), chiffresEnLettres.get(chiffreUnites));
                else
                    enLettres += String.join("-", dizainesIrreguliers.get(chiffreDizaines), chiffresEnLettres.get(chiffreUnites + 10));
            } else if (dizainesReguliers.containsKey(chiffreDizaines))
                enLettres += composerNombre(nombre, dizainesReguliers.get(chiffreDizaines));
            else
                enLettres += "dix-" + chiffresEnLettres.get(chiffreUnites);
        }
        if (nombreToString.endsWith("80")) enLettres += "s";
        return enLettres;
    }

    private int getChiffre(String nombre, POSITION position) {
        return switch (position) {
            case CENTAINES -> Integer.parseInt(String.valueOf(nombre.charAt(0)));
            case DIZAINES -> Integer.parseInt(String.valueOf(nombre.charAt(nombre.length() - 2)));
            case UNITES -> Integer.parseInt(String.valueOf(nombre.charAt(nombre.length() - 1)));
        };
    }

    private String composerNombre(int i, String correspondanceLettres) {
        if (String.valueOf(i).endsWith("0"))
            return correspondanceLettres;
        if (String.valueOf(i).endsWith("1"))
            return correspondanceLettres + "-et-un";
        return correspondanceLettres + "-" + chiffresEnLettres.get(Integer.parseInt(String.valueOf(String.valueOf(i).charAt(String.valueOf(i).length() - 1))));
    }
}
